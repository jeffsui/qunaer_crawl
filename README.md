# qunaer_crawl

去哪儿的爬虫练习

## 核心文件 
- crawl.py
- DbUtil.py
- requirements.txt

## 本地环境初始化

### 1. 克隆项目 
`git clone https://gitlab.com/jeffsui/qunaer_crawl.git`

### 2. 安装依赖 
`pip install -r requirements.txt`

### 3. 其他

#### 3.1 导出依赖 
1. 安装pipreqs

    `pip install pipreqs`
    
2. 导出requriements.txt文件

    在windows中,终端切换到项目所在的文件夹下:
    运行:
    `pipreqs ./`
    如果遇到如下错误:
    
    `UnicodeDecodeError: 'gbk' codec can't decode byte 0x80 in position 776: illegal multibyte sequence`
    解决方法:
    `pipreqs ./  --encoding=utf8`

3. 安装依赖模块

    `pip install -r requriements.txt `

### 4.如何运行爬虫

#### 4.1 修改 `crawl.py` main方法中 search_key 的值为你要检索的城市名

#### 4.2 运行下面的命令 

```python
python crawl.py 
```



