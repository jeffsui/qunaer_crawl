#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2019-08-07 07:54:10
# @Author  : Jeff.Sui (you@example.org)
# @Link    : http://jeffsui.github.io
# @Version : $Id$

import pymysql
from pymysql.cursors import DictCursor

class DbUtil(object):
    _db = None

    _config = {
        'host':"localhost",
        'port':3306,
        'username':"root",
        'password':'root',
        'database':"qunaer",
        'charset':"utf8"
    }

    def __connect(self):
        if self._db == None:
            self._db = pymysql.connect(
                host = self._config['host'],
                port = self._config['port'],
                user = self._config['username'],
                passwd = self._config['password'],
                db = self._config['database'],
                charset = self._config['charset']
            )
        return self._db

    def __init__(self):
        self.__connect()

    def __del__(self):
        if(self._db is not None):
            self._db.close()

    def query(self,_sql):
        cursor = self.__connect().cursor()
        try:
            cursor.execute(_sql)
            data = cursor.fetchall()
            #提交到数据库执行
            self.__connect().commit()
        except:
            #如果发生错误则回滚
            self.__connect().rollback()
            return False
        return data
    def queryone(self,_sql):
        cursor = self.__connect().cursor(DictCursor)
        try:
            cursor.execute(_sql)
            data = cursor.fetchone()
            #提交到数据库执行
            self.__connect().commit()
        except:
            #如果发生错误则回滚
            self.__connect().rollback()
            return False
        return data
    def update(self,_sql,data):
        cursor = self.__connect().cursor()
        try:
            cols = ", ".join('`{}`'.format(k) for k in data.keys())
            # print(cols)
            val_cols = ', '.join('%({})s'.format(k) for k in data.keys())
            # print(val_cols)
            res_sql = _sql %(cols,val_cols)
            # print(res_sql)
            cursor.execute(res_sql,data)
            #提交到数据库执行
            self.__connect().commit()
        except:
            #如果发生错误则回滚
            self.__connect().rollback()
            return False
        return cursor.rowcount


if __name__ == '__main__':
    import datetime
    now =  datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    data = {'s_name': '老虎滩海洋公园','s_price': '50', 's_address': '地址：辽宁省大连市中山区滨海中路9号', 's_level': '0.9', 's_desc': '一揽湾区山海，遍赏极地海洋', 's_month_sold': '7934','s_add_time':f'{now}'}
    db = DbUtil()
    # one = db.queryone('select * from sightinfo where s_id =1')
    # print(one)
    # all = db.query('select * from sightinfo')
    # for k in all:
    #     print(k)
    sql = "insert into sightinfo(%s) values(%s)"
    try:
        # conn = pymysql.connect(host='localhost',port=3306,user='root',passwd='root',db='qunaer')
        # cur = conn.cursor()
        # row = cur.execute(sql,data)
        row =db.update(sql,data)
        print(row)
    except Exception as e:
        raise e
    #     
    #     
    '''   
    try:
        sql = "select * from sightinfo where s_id=1"
        conn = pymysql.connect(host='localhost',port=3306,user='root',passwd='root',db='qunaer')
        cur = conn.cursor()
        cur.execute(sql)
        result = cur.fetchone()
        print(result)
    except Exception as e:
        raise e
    '''
