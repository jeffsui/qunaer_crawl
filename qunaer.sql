/*
Navicat MySQL Data Transfer

Source Server         : mylocaldb
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : qunaer

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2019-08-07 11:06:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sightinfo`
-- ----------------------------
DROP TABLE IF EXISTS `sightinfo`;
CREATE TABLE `sightinfo` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '景点id',
  `s_name` varchar(64) DEFAULT NULL COMMENT '景点名称',
  `s_price` double(11,0) DEFAULT NULL COMMENT '景点价格',
  `s_address` varchar(120) DEFAULT NULL COMMENT '景点地址',
  `s_level` double(10,2) DEFAULT NULL COMMENT '景点热度',
  `s_desc` varchar(128) DEFAULT NULL COMMENT '景点描述',
  `s_month_sold` int(11) DEFAULT NULL COMMENT '景点上月销售额',
  `s_city` varchar(32) DEFAULT NULL COMMENT '景点城市',
  `s_add_time` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;