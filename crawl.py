#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2019-08-06 19:50:00
# @Author  : JeffSui ()
# @Link    : http://jeffsui.github.io
# @Version : $Id$


import requests
from lxml import etree
import time
import datetime
from DbUtil import DbUtil
headers={
    'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.81 Safari/537.36'
}

db = DbUtil()

def get_max_pages(urls,headers=headers):
    """
    获取最大分页数
    :param urls:　抓取页面地址
    :param headers:　请求头
    :return:　返回最大页面数
    """
    res = requests.get(urls,headers)
    root = etree.HTML(res.content)
    max_pages = root.xpath('//*[@id="pager-container"]/div/a[8]/text()')
    return int(max_pages[0])

def get_content_from(element,begin_key,end_key="",end=-1):
    content = element[0].xpath('string(.)')
    # print(content)
    if end_key:
        start = content.find(begin_key)
        end = content.find(end_key)
        return content[start+1:end-1]
    else:
        start = content.rfind(begin_key)
        return content[start+1:]

def save_to_csv(fileName ='',dataDict={}):
    """
    导出csv方法
    :param fileName: 文件名
    :param dataDict: 爬虫爬取的字典类型的数据
    :return: 生成csv文件
    """
    with open(fileName, 'a+',encoding="utf-8") as f:  # Just use 'w' mode in 3.x
        for value in dataDict.values():
            f.write(value+",")
        f.write("\n")


def crawl(url,city,headers=headers):
    """
    爬虫核心类
    :param url: 爬取页面url地址
    :param city: 城市
    :param headers: 请求头
    :return:
    """
    page = 1
    totalPage = get_max_pages(url)
    while page<totalPage+1:
        sight_info={}
        response = requests.get(f'{url}?keyword={keyword}&region=&from=mpl_search_suggest&page={page}',headers=headers)
        root = etree.HTML(response.content)
        elements = root.xpath('//*[@id="search-list"]')
        for element in elements:
            for i in range(1,16):
                sight_info['s_name'] = element.xpath(f'./div[{i}]/div/div[2]/h3/a/text()')[0]
                                                    # //*[@id="search-list"]/div[2]/div/div[3]
                sight_info['s_price'] = get_content_from(element.xpath(f'./div[{i}]/div/div[3]'),u'¥',u'起')
                sight_info['s_address'] = element.xpath(f'./div[{i}]/div[1]/div[2]/div/p/span/text()')[0]
                sight_info['s_level'] = get_content_from(element.xpath(f'./div[{i}]/div[1]/div[2]/div/div[1]/div'),u"度 ",end_key=u"度：")
                                                # //*[@id="search-list"]/div[1]/div/div[2]/div/p/span
                                                # //*[@id="search-list"]/div[14]/div/div[2]/div/p/span
                                                # //*[@id="search-list"]/div[13]/div/div[2]/div/p/span
                if element.xpath(f'./div[{i}]/div/div[2]/div/div[2]/text()'):
                    sight_info['s_desc'] = element.xpath(f'./div[{i}]/div/div[2]/div/div[2]/text()')[0]
                else:
                    sight_info['s_desc'] = "no desc"
                sight_info['s_month_sold'] = get_content_from(element.xpath(f'./div[{i}]/div/div[3]'),'：',"",-1)
                sight_info['s_add_time']  = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                print(sight_info)
                time.sleep(1)
                sql = "insert into sightinfo(%s) values(%s)"
                result = db.update(sql,sight_info)
                # print("insert ",result)
        time.sleep(1)
        page+=1
    # return sight_info

if __name__ == '__main__':
    # keyword=u"大连辽宁"
    urls = f"https://piao.qunar.com/ticket/list.htm"
    data = crawl(urls,city=u"辽宁大连",headers=headers)


    